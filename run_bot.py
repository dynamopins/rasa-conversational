from rasa_core.channels import HttpInputChannel
from rasa_core.agent import Agent
from rasa_core.interpreter import RasaNLUInterpreter
from rasa_rocket_connector import RocketChatInput
from rasa_core import utils
import argparse

import logging

logger = logging.getLogger(__name__)


def create_argument_parser():
    """Parse all the command line arguments for the server script."""
    parser = argparse.ArgumentParser(
        description='starts server to serve an agent')
    parser.add_argument(
        '-p', '--port',
        type=int,
        default=5005,
        help="port to run the server at")
    
    parser.add_argument(
        '-o', '--log_file',
        type=str,
        default="rasa_core.log",
        help="store log file in specified file")
    
    utils.add_logging_option_arguments(parser)
    return parser


if __name__ == '__main__':
    # Running as standalone python application

    arg_parser = create_argument_parser()
    cmdline_args = arg_parser.parse_args()
    utils.configure_colored_logging(cmdline_args.loglevel)
    utils.configure_file_logging("DEBUG", "rasa_core.log")

    logger.info("Rasa process starting")
    
    nlu_interpreter = RasaNLUInterpreter('./models/nlu/default/weathernlu')
    agent = Agent.load('./models/dialogue', interpreter=nlu_interpreter)
    logger.info("Finished loading agent, starting input channel & server.")

    input_channel = RocketChatInput(
        'http://localhost:3000/hooks/jCiuG3gcKfD4qF9mr/L8uQaRLaNFT24dGbvMsBvgvJu6topG9v6a54C9fkfmaF9k2K', True)
    
    agent.handle_channel(HttpInputChannel(5004, '/', input_channel))
