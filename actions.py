﻿from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from rasa_core.actions.forms import FormAction, EntityFormField
from rasa_core.events import SlotSet


class ActionWeather(FormAction):
    
    @staticmethod
    def required_fields():
        return [
            EntityFormField("location", "location"),
        ]
    
    def name(self):
        return 'action_weather'
    
    def run(self, dispatcher, tracker, domain):
        from apixu.client import ApixuClient
        api_key = "1cd0d8438fea41ffbfb114608181306"
        client = ApixuClient(api_key)
        
        test = tracker.get_slot('condition')
        print(test)
        
        loc = tracker.get_slot('location')
        current = client.getCurrentWeather(q=loc, lang='de')
        print('test')
        print(tracker.as_dialogue())
        print('#######################')
        print(tracker.export_stories())
        
        country = current['location']['country']
        city = current['location']['name']
        condition = current['current']['condition']['text']
        temperature_c = current['current']['temp_c']
        humidity = current['current']['humidity']
        wind_mph = current['current']['wind_mph']
        
        response = """Momentan ist es {} in {}. Die Temperatur beträgt {} Grad.""" \
            .format(condition, city, temperature_c)
        dispatcher.utter_message(response)
        return [SlotSet('location', loc)]
